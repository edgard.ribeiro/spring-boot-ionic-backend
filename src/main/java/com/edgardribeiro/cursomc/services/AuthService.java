package com.edgardribeiro.cursomc.services;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.edgardribeiro.cursomc.domain.Cliente;
import com.edgardribeiro.cursomc.repositories.ClienteRepository;
import com.edgardribeiro.cursomc.services.exceptions.ObjectNotFoundException;

@Service
public class AuthService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private BCryptPasswordEncoder pe;
	
	@Autowired
	private EmailService emailService;
	
	private Random rand = new Random();
	
	public void sendNewPassword(String email)  {
		Cliente cliente = clienteRepository.findByEmail(email);
		if(cliente == null) {
			throw new ObjectNotFoundException("Email não encontrado");
		}
		
		String newPass = newPassword();
		cliente.setSenha(pe.encode(newPass));
		
		clienteRepository.save(cliente);
		emailService.sendNewPasswordEmail(cliente, newPass);
	}

	private String newPassword() {
		char[] vet = new char[10];
		for(int i = 0; i < 10; i++) {
			vet[i] = randomChar();
		}
		return new String(vet);
	}

	private char randomChar() {
		int opt = rand.nextInt(3);
		if(opt == 0) { // gera digito
			return (char) (rand.nextInt(10) + 48); // esse 48 representa o numero 0. Acessar o site https://www.tamasoft.co.jp/en/general-info/unicode-decimal.html
		}else if(opt == 1) { // gera letra maiúscula
			return (char) (rand.nextInt(26) + 65); // esse 65 representa o numero 0. Acessar o site https://www.tamasoft.co.jp/en/general-info/unicode-decimal.html
		}else { // gera letra minúscula
			return (char) (rand.nextInt(26) + 97); // esse 97 representa o numero 0. Acessar o site https://www.tamasoft.co.jp/en/general-info/unicode-decimal.html
		}
	}

}
